import { Employee } from "./employee";
import { ILocation } from "./ilocation";

export class CompanyLocationArray implements ILocation {
  private persons: Employee[] = [];

  addPerson(person: Employee): void {
    this.persons.push(person);
  }

  getPerson(index: number): Employee | null {
    return this.persons[index] || null;
  }

  getCount(): number {
    return this.persons.length;
  }

  getPersons(): Employee[] {
    return this.persons;
  }
}
