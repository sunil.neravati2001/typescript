export class Employee {
  private name: string;
  private project: string;

  constructor(name: string, project: string) {
    this.name = name;
    this.project = project;
  }

  get getCurrentProject(): string {
    return this.project;
  }

  get getName(): string {
    return this.name;
  }
}
