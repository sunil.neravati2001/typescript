export class CompanyLocationArray {
    constructor() {
        this.persons = [];
    }
    addPerson(person) {
        this.persons.push(person);
    }
    getPerson(index) {
        return this.persons[index] || null;
    }
    getCount() {
        return this.persons.length;
    }
    getPersons() {
        return this.persons;
    }
}
