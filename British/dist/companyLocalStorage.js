import { Employee } from "./employee.js";
export class CompanyLocationLocalStorage {
    constructor(localStorageKey) {
        this.localStorageKey = localStorageKey;
        this.persons = JSON.parse(localStorage.getItem(this.localStorageKey) || '[]', (key, value) => {
            if (key === "" && Array.isArray(value)) {
                return value.map(employeeData => new Employee(employeeData.name, employeeData.project));
            }
            return value;
        });
    }
    addPerson(person) {
        this.persons.push(person);
        localStorage.setItem(this.localStorageKey, JSON.stringify(this.persons));
    }
    getPerson(index) {
        return this.persons[index] || null;
    }
    getCount() {
        return this.persons.length;
    }
    getPersons() {
        return this.persons;
    }
}
