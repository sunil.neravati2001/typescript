import { IEmployee } from "./iemployee";

export class Backend implements IEmployee {
  constructor(public name: string, public projectName: string) {}
  getCurrentProject(): string {
    return this.projectName;
  }
  getName(): string {
    return this.name;
  }
}
