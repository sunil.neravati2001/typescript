import { IEmployee } from "./iemployee";

export class Company {
  private employees: IEmployee[] = [];

  public add(employee: IEmployee): IEmployee {
    this.employees.push(employee);
    return employee;
  }

  getListOfEmployeeProjects(): string[] {
    return this.employees.map((emp) => emp.getCurrentProject());
  }

  getNameList(): string[] {
    return this.employees.map((emp) => emp.getName());
  }
}
