import { Company } from "./company";
import { Frontend } from "./frontend";
import { Backend } from "./backend";

const americanCompany = new Company();
const frontendEmployee = new Frontend("himeshzakkam", "typescript");
const backendEmployee = new Backend("midhun", "java");

console.log(americanCompany.add(frontendEmployee));
console.log(americanCompany.add(backendEmployee));
console.log(americanCompany.getListOfEmployeeProjects());
console.log(americanCompany.getNameList());
