export class Backend {
    constructor(name, projectName) {
        this.name = name;
        this.projectName = projectName;
    }
    getCurrentProject() {
        return this.projectName;
    }
    getName() {
        return this.name;
    }
}
