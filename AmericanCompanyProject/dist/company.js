export class Company {
    constructor() {
        this.employees = [];
    }
    add(employee) {
        this.employees.push(employee);
        return employee;
    }
    getListOfEmployeeProjects() {
        return this.employees.map((emp) => emp.getCurrentProject());
    }
    getNameList() {
        return this.employees.map((emp) => emp.getName());
    }
}
