import { Company } from "./company";
import { Frontend } from "./frontend";
import { Backend } from "./backend";

const europeanCompany = new Company();
const frontendEmployee = new Frontend("himesh", "typescript");
const backendEmployee = new Backend("midhun", "java");

console.log(europeanCompany.add(frontendEmployee));
console.log(europeanCompany.add(backendEmployee));
console.log(europeanCompany.getListOfEmployeeProjects());
console.log(europeanCompany.getNameList());
