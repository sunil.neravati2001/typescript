export class Employee {
  constructor(private name: string, private projectName: string) {}

  getCurrentProject(): string {
    return this.projectName;
  }

  getName(): string {
    return this.name;
  }
}
