import { Employee } from "./employee";

export class Frontend extends Employee {
  constructor(name: string, projectName: string) {
    super(name, projectName);
  }
}
